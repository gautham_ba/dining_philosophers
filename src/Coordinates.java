/**
 * Created by gauth_000 on 27-Sep-15.
 */
class Coordinates
{
    // Imagianry pentagon is 600*600, philosophers and sticks will jut a little beyond that
    // Make screen 700*700 and see how it looks, adjust using below constants only for the code to adapt itself
    final static int SCREEN_WIDTH = 800, SCREEN_HEIGHT = 800;
    final static int PHIL_NUM = 5;

    final static int PENT_RADIUS = 300;
    final static int PHIL_RADIUS = PENT_RADIUS / 5;
    final static int STICK_HALF_LEN = PHIL_RADIUS;

    // adjusts center of pentagon to top-left origin of Java frame
    final static int OFFSET_X = SCREEN_WIDTH / 2; // ensures center is centered horizontally
    final static int OFFSET_Y = SCREEN_HEIGHT / 2; // ensures center is centered vertically

    public static final int STROKE_WIDTH=5;

    public static int getX(int x)
    {
        return OFFSET_X + x;
    }

    public static int getY(int y)
    {
        return OFFSET_Y - y;
    }

    public static int[][][] genSticks()
    {
        // Dimensions: index of stick, 2 points for each stick, x and y of each point
        int[][][] sticks = new int[PHIL_NUM][2][2];
        final int offset = 36; // degrees
        for (int i = 0, step = 360 / PHIL_NUM; i < PHIL_NUM; i++)
        {
            double angle = Math.toRadians(offset + i * step);
            double sin = Math.sin(angle);
            double cos = Math.cos(angle);
            int innerRadius = PENT_RADIUS - STICK_HALF_LEN;
            int outerRadius = PENT_RADIUS + STICK_HALF_LEN;
            // point closer to center of pentagon
            // 	x
            sticks[i][0][0] = (int) Math.round(innerRadius * sin);
            // 	y
            sticks[i][0][1] = (int) Math.round(innerRadius * cos);
            // point further form center of pentagon
            // 	x
            sticks[i][1][0] = (int) Math.round(outerRadius * sin);
            //	y
            sticks[i][1][1] = (int) Math.round(outerRadius * cos);
        }
        return sticks;
    }

    public static int[][] genPhil()
    {
        int prof[][] = new int[PHIL_NUM][2];
        for (int i = 0, step = 360 / PHIL_NUM; i < PHIL_NUM; i++)
        {
            double angle = Math.toRadians(i * step);
            // y
            prof[i][1] = (int) Math.round(PENT_RADIUS * Math.cos(angle));
            // x
            prof[i][0] = (int) Math.round(PENT_RADIUS * Math.sin(angle));
        }
        return prof;
    }
}
