/**
 * Created by gauth_000 on 04-Oct-15.
 */
public class Constants
{
    public static final int PICK_LEFT=0;
    public static final int PICK_RIGHT=1;
    public static final int UNPICK_LEFT=2;
    public static final int UNPICK_RIGHT=3;
    public static final int KILL_PROCESS=4;
}
