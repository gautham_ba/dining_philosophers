import javax.swing.*;

/**
 * Created by gauth_000 on 27-Sep-15.
 */
public class Philosophers extends JFrame
{
    public Philosophers()
    {
        super("Dining philosophers");
        setSize(Coordinates.SCREEN_WIDTH, Coordinates.SCREEN_HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        PhilosophersPane philosophersPane = new PhilosophersPane();
        getContentPane().add(philosophersPane);
        show();
    }

    public static void main(String[] args)
    {
        Philosophers philosophers = new Philosophers();
    }
}

