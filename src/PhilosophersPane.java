import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by gauth_000 on 27-Sep-15.
 */
class PhilosophersPane extends JPanel implements Runnable
{
    private int p;
    private int action;
    private int[][] philosophers;
    private int[][][] sticks;
    private Graphics2D comp2D;
    private Color[] philosophersColour;
    private Color[] sticksColour;
    private ArrayList<Integer> philosophersActionsList;
    private ArrayList<Integer> sticksActionsList;

    public PhilosophersPane()
    {
        philosophers=Coordinates.genPhil();
        sticks=Coordinates.genSticks();
        philosophersColour =new Color[] {Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW, Color.ORANGE};
        sticksColour =new Color[] {Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK};
        philosophersActionsList=new ArrayList<Integer>();
        sticksActionsList=new ArrayList<Integer>();
        Thread runner=new Thread(this);

        runner.start();
    }

    public void paintComponent(Graphics comp)
    {
        super.paintComponent(comp);
        Graphics2D comp2D = (Graphics2D) comp;

        comp2D.setStroke(new BasicStroke(Coordinates.STROKE_WIDTH));
        this.comp2D=comp2D;

        for(int i=0; i<Coordinates.PHIL_NUM; ++i)
        {
            comp2D.setColor(philosophersColour[i]);
            drawCenteredCircle(
                    Coordinates.getX(philosophers[i][0]),
                    Coordinates.getY(philosophers[i][1]),
                    Coordinates.PHIL_RADIUS);
        }

        for(int i=0; i<Coordinates.PHIL_NUM; ++i)
        {
            comp2D.setColor(sticksColour[i]);
            comp2D.drawLine(Coordinates.getX(sticks[i][0][0]), Coordinates.getY(sticks[i][0][1]),
                    Coordinates.getX(sticks[i][1][0]), Coordinates.getY(sticks[i][1][1]));
        }

        action();
    }

    public void drawCenteredCircle(int x, int y, int r)
    {
        x = x - (r / 2);
        y = y - (r / 2);
        comp2D.fillOval(x, y, r, r);
    }

    public void action()
    {
        switch (action)
        {
            case Constants.PICK_RIGHT:
                comp2D.setColor(philosophersColour[p]);
                comp2D.drawLine
                        (
                            Coordinates.getX(sticks[(p-1+Coordinates.PHIL_NUM)%Coordinates.PHIL_NUM][0][0]),
                            Coordinates.getY(sticks[(p-1+Coordinates.PHIL_NUM)%Coordinates.PHIL_NUM][0][1]),
                            Coordinates.getX(sticks[(p-1+Coordinates.PHIL_NUM)%Coordinates.PHIL_NUM][1][0]),
                            Coordinates.getY(sticks[(p-1+Coordinates.PHIL_NUM)%Coordinates.PHIL_NUM][1][1])
                        );
                break;
            case Constants.UNPICK_RIGHT:
                comp2D.setColor(Color.BLACK);
                comp2D.drawLine
                        (
                                Coordinates.getX(sticks[(p - 1 + Coordinates.PHIL_NUM) % Coordinates.PHIL_NUM][0][0]),
                                Coordinates.getY(sticks[(p - 1 + Coordinates.PHIL_NUM) % Coordinates.PHIL_NUM][0][1]),
                                Coordinates.getX(sticks[(p - 1 + Coordinates.PHIL_NUM) % Coordinates.PHIL_NUM][1][0]),
                                Coordinates.getY(sticks[(p - 1 + Coordinates.PHIL_NUM) % Coordinates.PHIL_NUM][1][1])
                        );
                break;
            case Constants.PICK_LEFT:
                comp2D.setColor(philosophersColour[p]);
                comp2D.drawLine
                        (
                            Coordinates.getX(sticks[p][0][0]),
                            Coordinates.getY(sticks[p][0][1]),
                            Coordinates.getX(sticks[p][1][0]),
                            Coordinates.getY(sticks[p][1][1])
                        );
                break;
            case Constants.UNPICK_LEFT:
                comp2D.setColor(Color.BLACK);
                comp2D.drawLine
                        (
                            Coordinates.getX(sticks[p][0][0]),
                            Coordinates.getY(sticks[p][0][1]),
                            Coordinates.getX(sticks[p][1][0]),
                            Coordinates.getY(sticks[p][1][1])
                        );
                break;
            case Constants.KILL_PROCESS:
                comp2D.setColor(Color.GRAY);
                drawCenteredCircle
                        (
                            Coordinates.getX(philosophers[p][0]),
                            Coordinates.getY(philosophers[p][1]),
                            Coordinates.PHIL_RADIUS
                        );
                break;
        }
    }

    @Override
    public void run()
    {
        Scanner keyboard=new Scanner(System.in);
        while(true)
        {
            p=keyboard.nextInt();
            action=keyboard.nextInt();

            switch (action)
            {
                case 0:
                    sticksColour[p]=philosophersColour[p];
                    break;
                case 1:
                    sticksColour[(p-1+Coordinates.PHIL_NUM)%Coordinates.PHIL_NUM]=philosophersColour[(p-1+Coordinates.PHIL_NUM)%Coordinates.PHIL_NUM];
                    break;
                case 2:
                    sticksColour[p]=Color.BLACK;
                    break;
                case 3:
                    sticksColour[(p-1+Coordinates.PHIL_NUM)%Coordinates.PHIL_NUM]=Color.BLACK;
                    break;
                case 4:
                    philosophersColour[p]=Color.GRAY;
                    break;
            }
            repaint();
        }
    }
}
